package edu.uclm.esi.disoft.patrones.ejercicio0a;

import java.io.Serializable;

public interface Figura extends Serializable {

	double getArea();

}
