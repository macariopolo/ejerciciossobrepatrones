package edu.uclm.esi.disoft.patrones.ejercicio0a;

public class Circulo implements Figura {

	private double radio;

	public Circulo(double radio) {
		this.radio=radio;
	}

	@Override
	public double getArea() {
		return Math.PI*radio*radio;
	}

}
