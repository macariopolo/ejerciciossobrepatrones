package edu.uclm.esi.disoft.patrones.ejercicio0a;

public class LanzadoraEjercicio0a {

	public static void main(String[] args) throws Exception {
		Figura a=new Cuadrado(5.0);
		Figura b=new Circulo(3.5);
		Figura c=new Rectangulo(6, 8);
		
		Collage collage=new Collage(a, b, c);
		collage.save();
		System.out.println(collage.getArea());
		
		Collage copia=Collage.load();
		System.out.println(copia.getArea());
	}

}
