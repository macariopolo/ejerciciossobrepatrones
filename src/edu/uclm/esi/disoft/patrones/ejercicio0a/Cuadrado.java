package edu.uclm.esi.disoft.patrones.ejercicio0a;

public class Cuadrado implements Figura {

	private double lado;

	public Cuadrado(double lado) {
		this.lado=lado;
	}

	@Override
	public double getArea() {
		return lado*lado;
	}

}
