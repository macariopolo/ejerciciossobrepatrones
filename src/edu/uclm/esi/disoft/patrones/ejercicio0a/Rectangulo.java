package edu.uclm.esi.disoft.patrones.ejercicio0a;

public class Rectangulo implements Figura {
	
	private double a;
	private double b;

	public Rectangulo(double a, double b) {
		this.a=a;
		this.b=b;
	}

	@Override
	public double getArea() {
		return Math.abs(a)*Math.abs(b);
	}

}
