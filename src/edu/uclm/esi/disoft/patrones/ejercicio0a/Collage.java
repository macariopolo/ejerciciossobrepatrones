package edu.uclm.esi.disoft.patrones.ejercicio0a;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Collage implements Serializable {

	private Figura[] figuras;

	public Collage(Figura... figuras) {
		this.figuras=figuras;
	}

	public double getArea() {
		double r=0;
		for (Figura figura : figuras)
			r+=figura.getArea();
		return r;
	}

	public void save() throws Exception {
		String directorioTemporal=System.getProperty("java.io.tmpdir");
		FileOutputStream fos=new FileOutputStream(directorioTemporal + "collage.serializado");
		ObjectOutputStream oos=new ObjectOutputStream(fos);
		oos.writeObject(this);
		fos.close();
	}

	public static Collage load() throws Exception {
		String directorioTemporal=System.getProperty("java.io.tmpdir");
		FileInputStream fis=new FileInputStream(directorioTemporal + "collage.serializado");
		ObjectInputStream ois=new ObjectInputStream(fis);
		Collage collage = (Collage) ois.readObject();
		ois.close();
		return collage;
	}

}
