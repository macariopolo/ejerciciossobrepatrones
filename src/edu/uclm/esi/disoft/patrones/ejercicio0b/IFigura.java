package edu.uclm.esi.disoft.patrones.ejercicio0b;

public interface IFigura {
	double getArea();
}
