package edu.uclm.esi.disoft.patrones.ejercicio0b;

public class Circulo implements IFigura {

	private double radio;

	public Circulo(double radio) {
		this.radio=radio;
	}

	@Override
	public double getArea() {
		return Math.PI*radio*radio;
	}

}
