package edu.uclm.esi.disoft.patrones.ejercicio0b;

public class Cuadrado implements IFigura {

	private double lado;

	public Cuadrado(double lado) {
		this.lado=lado;
	}

	@Override
	public double getArea() {
		return lado*lado;
	}

}
