package edu.uclm.esi.disoft.patrones.ejercicio0b;

import java.util.Vector;

public class GrupoDeFiguras implements IFigura {
	private Vector<IFigura> figuras;
	
	public GrupoDeFiguras() {
		this.figuras=new Vector<>();
	}
	
	@Override
	public double getArea() {
		double r=0;
		for (IFigura f : figuras)
			r+=f.getArea();
		return r;
	}

}
