package edu.uclm.esi.disoft.patrones.ejercicioMetamodelo;

import java.util.Vector;

public class Clase extends Contenedor {
	private Clase superclase;
	private boolean abstracta;
	private Vector<Campo> campos;
	private Vector<Constructor> constructores;
	
	public Clase(String nombre, Clase superclase, boolean abstracta, Interfaz...superinterfaces) {
		super(nombre, superinterfaces);
		this.superclase=superclase;
		this.abstracta=abstracta;
		this.campos=new Vector<>();
		this.constructores=new Vector<>();
		this.metodos=new Vector<>();
	}
	
	public void add(Campo campo) {
		this.campos.add(campo);
	}
	
	public void add(Constructor constructor) {
		this.constructores.add(constructor);
	}
}
