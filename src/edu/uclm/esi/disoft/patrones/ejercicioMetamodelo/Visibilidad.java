package edu.uclm.esi.disoft.patrones.ejercicioMetamodelo;

public enum Visibilidad {
	PUBLICO, PROTEGIDO, PRIVADO, PAQUETE;
}
