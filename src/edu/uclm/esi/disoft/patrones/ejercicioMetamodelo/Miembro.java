package edu.uclm.esi.disoft.patrones.ejercicioMetamodelo;

public abstract class Miembro extends Clasificador {
	protected Visibilidad visibilidad;
	
	public Miembro(Visibilidad visibilidad, String nombre) {
		super(nombre);
		this.visibilidad=visibilidad;
	}
}
