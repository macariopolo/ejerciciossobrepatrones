package edu.uclm.esi.disoft.patrones.ejercicioMetamodelo;

public class Interfaz extends Contenedor {
	
	public Interfaz(String nombre, Interfaz... superinterfaces) {
		super(nombre, superinterfaces);
	}
}
