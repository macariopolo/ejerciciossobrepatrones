package edu.uclm.esi.disoft.patrones.ejercicioMetamodelo;

public class Constructor extends Operacion {

	public Constructor(Visibilidad visibilidad, String nombre, Parametro... parametros) {
		super(visibilidad, nombre, parametros);
	}

}
