package edu.uclm.esi.disoft.patrones.ejercicioMetamodelo;

public class Campo extends Miembro {

	private Clase tipo;
	private String multiplicidad;

	public Campo(Visibilidad visibilidad, String nombre, Clase tipo, String multiplicidad) {
		super(visibilidad, nombre);
		this.tipo=tipo;
		this.multiplicidad=multiplicidad;
	}

}
