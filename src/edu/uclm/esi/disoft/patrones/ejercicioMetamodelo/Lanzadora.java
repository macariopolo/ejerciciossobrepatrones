package edu.uclm.esi.disoft.patrones.ejercicioMetamodelo;

public class Lanzadora {

public static void main(String[] args) {
	DiagramaDeClases dc=new DiagramaDeClases();
	
	Interfaz iFigura=new Interfaz("IFigura", null);
	Clase doble=new Clase("double", null, false, null);
	Metodo getAreaFigura=new Metodo(Visibilidad.PUBLICO, "getArea", doble, true, null);
	iFigura.add(getAreaFigura);
	
	Clase circulo=new Clase("Circulo", null, false, iFigura);
	Campo radio=new Campo(Visibilidad.PRIVADO, "radio", doble, "1");
	Parametro pRadio=new Parametro("radio", doble);
	Constructor cCirculo=new Constructor(Visibilidad.PUBLICO, "Circulo", pRadio);
	Metodo getAreaCirculo=new Metodo(Visibilidad.PUBLICO, "getArea", doble, false, null);
	circulo.add(radio);
	circulo.add(cCirculo);
	circulo.add(getAreaCirculo);
	
	Clase cuadrado=new Clase("Cuadrado", null, false, iFigura);
	Campo lado=new Campo(Visibilidad.PRIVADO, "lado", doble, "1");
	Parametro pLado=new Parametro("lado", doble);
	Constructor cCuadrado=new Constructor(Visibilidad.PUBLICO, "Cuadrado", pLado);
	Metodo getAreaCuadrado=new Metodo(Visibilidad.PUBLICO, "getArea", doble, false, null);
	cuadrado.add(lado);
	cuadrado.add(cCuadrado);
	cuadrado.add(getAreaCuadrado);
	
	Clase rectangulo=new Clase("Rectangulo", null, false, iFigura);
	Campo lado1=new Campo(Visibilidad.PRIVADO, "lado1", doble, "1");
	Campo lado2=new Campo(Visibilidad.PRIVADO, "lado2", doble, "1");
	Parametro pLado1=new Parametro("lado1", doble);
	Parametro pLado2=new Parametro("lado2", doble);
	Constructor cRectangulo=new Constructor(Visibilidad.PUBLICO, "Rectangulo", pLado1, pLado2);
	Metodo getAreaRectangulo=new Metodo(Visibilidad.PUBLICO, "getArea", doble, false, null);
	rectangulo.add(lado1);
	rectangulo.add(lado2);
	rectangulo.add(cRectangulo);
	rectangulo.add(getAreaRectangulo);
	
	dc.add(iFigura);
	dc.add(circulo);
	dc.add(cuadrado);
	dc.add(rectangulo);
	
	dc.printCode();
}

}
