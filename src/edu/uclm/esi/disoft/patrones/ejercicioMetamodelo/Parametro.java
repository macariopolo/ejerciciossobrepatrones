package edu.uclm.esi.disoft.patrones.ejercicioMetamodelo;

public class Parametro extends Clasificador {
	private Clase tipo;
	
	public Parametro(String nombre, Clase tipo) {
		super(nombre);
		this.tipo=tipo;
	}
}
