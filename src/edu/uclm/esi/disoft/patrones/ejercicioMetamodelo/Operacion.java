package edu.uclm.esi.disoft.patrones.ejercicioMetamodelo;

public abstract class Operacion extends Miembro {
	
	protected Parametro[] parametros;

	public Operacion(Visibilidad visibilidad, String nombre, Parametro... parametros) {
		super(visibilidad, nombre);
		this.parametros=parametros;
	}

}
