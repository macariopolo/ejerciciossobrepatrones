package edu.uclm.esi.disoft.patrones.ejercicioMetamodelo;

import java.util.Vector;

public abstract class Contenedor extends Clasificador {
	protected Interfaz[] interfaces;
	protected Vector<Metodo> metodos;
	
	public Contenedor(String nombre, Interfaz... superinterfaces) {
		super(nombre);
		this.interfaces=superinterfaces;
		this.metodos=new Vector<>();
	}
	
	public void add(Metodo metodo) {
		this.metodos.add(metodo);
	}
}
