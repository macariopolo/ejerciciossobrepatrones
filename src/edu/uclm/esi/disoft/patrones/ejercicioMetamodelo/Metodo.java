package edu.uclm.esi.disoft.patrones.ejercicioMetamodelo;

public class Metodo extends Operacion {

	private Clase tipo;
	private boolean abstracto;

	public Metodo(Visibilidad visibilidad, String nombre, Clase tipo, boolean abstracto, Parametro... parametros) {
		super(visibilidad, nombre, parametros);
		this.tipo=tipo;
		this.abstracto=abstracto;
	}

}
