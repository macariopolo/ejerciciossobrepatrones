package edu.uclm.esi.disoft.patrones.ejercicio1;

import java.io.File;

public class LanzadoraEjercicio1 {
	private Directorio directorio;

	public LanzadoraEjercicio1(String path) throws Exception {
		File raiz=new File(path);
		if (!raiz.isDirectory()) 
			throw new Exception("La ruta " + path + " no corresponde a un directorio");
		this.directorio=new Directorio(raiz);
	}
	
	public static void main(String[] args) throws Exception {
		LanzadoraEjercicio1 a=new LanzadoraEjercicio1("/Users/macario.polo/Desktop/carpeta1");
		System.out.println(a.directorio.toJSON().toString());
		a.directorio.print(0);
	}
}
