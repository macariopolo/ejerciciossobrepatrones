package edu.uclm.esi.disoft.patrones.ejercicio1;

import java.io.File;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONObject;

public class Directorio extends Entrada {
	protected Vector<Entrada> entradas;
	
	public Directorio(File raiz) {
		super(raiz.getName());
		this.entradas=new Vector<>();
		File[] ficheros=raiz.listFiles();
		File fichero;
		Entrada entrada;
		for (int i=0; i<ficheros.length; i++) {
			fichero=ficheros[i];
			 if (fichero.isHidden())
				 continue;
			if (fichero.isFile()) 
				entrada=new Fichero(fichero.getName());
			else
				entrada=new Directorio(fichero);
			this.entradas.add(entrada);
		}
	}

	@Override
	public JSONObject toJSON() {
		JSONObject jso=new JSONObject();
		jso.put("tipo", "directorio");
		jso.put("nombre", nombre);
		JSONArray ficheros=new JSONArray();
		for (Entrada entrada : this.entradas) {
			ficheros.put(entrada.toJSON());
		}
		jso.put("ficheros", ficheros);
		return jso;
	}

	@Override
	public void print(int tabuladores) {
		super.print(tabuladores);
		for (Entrada entrada : this.entradas)
			entrada.print(tabuladores+1);
	}
}
