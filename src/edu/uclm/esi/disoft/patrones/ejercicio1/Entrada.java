package edu.uclm.esi.disoft.patrones.ejercicio1;

import org.json.JSONObject;

public abstract class Entrada {
	protected String nombre;
	
	public Entrada(String nombre) {
		this.nombre=nombre;
	}
	
	public void print(int tabuladores) {
		String r="";
		for (int i=0; i<tabuladores; i++)
			r+="\t";
		r+=nombre;
		System.out.println(r);
	}
	
	public abstract JSONObject toJSON();
}
