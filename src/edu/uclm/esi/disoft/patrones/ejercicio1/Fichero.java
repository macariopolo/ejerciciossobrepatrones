package edu.uclm.esi.disoft.patrones.ejercicio1;

import org.json.JSONObject;

public class Fichero extends Entrada {
	
	public Fichero(String nombre) {
		super(nombre);
	}

	@Override
	public JSONObject toJSON() {
		return new JSONObject().put("tipo", "fichero").put("nombre", nombre);
	}

}
