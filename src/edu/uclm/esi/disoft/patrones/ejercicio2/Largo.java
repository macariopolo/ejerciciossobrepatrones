package edu.uclm.esi.disoft.patrones.ejercicio2;

public class Largo extends ElementoListable {

	private long valor;

	public Largo(long valor) {
		this.valor=valor;
	}
	
	@Override
	protected String getValor() {
		return this.valor<=0 ? "(" + this.valor + ")" : "" + this.valor;
	}
}
