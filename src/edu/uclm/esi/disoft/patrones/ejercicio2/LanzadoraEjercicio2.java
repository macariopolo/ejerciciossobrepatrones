package edu.uclm.esi.disoft.patrones.ejercicio2;

public class LanzadoraEjercicio2 {
	
	public static void main(String[] args) {
		ElementoListable a=new Entero(1); 
		ElementoListable b=new Largo(2l);
		ElementoListable c=new Real(3.0f);
		ElementoListable d=new Doble(3.0);
		ElementoListable e=new Cadena("Paco");
		ElementoListable f=new Entero(-8);
		ElementoListable g=new Entero(5);
		ElementoListable h=new Cadena("Pérez");
		
		a.setSiguiente(b);
		b.setSiguiente(c);
		c.setSiguiente(d);
		d.setSiguiente(e);
		e.setSiguiente(f);
		f.setSiguiente(g);
		g.setSiguiente(h);
		
		System.out.println(a.getElementos());
		System.out.println(e.getElementos());
	}
}
