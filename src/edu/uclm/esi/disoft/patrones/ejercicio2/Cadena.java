package edu.uclm.esi.disoft.patrones.ejercicio2;

public class Cadena extends ElementoListable {

	private String valor;

	public Cadena(String valor) {
		this.valor=valor;
	}

	@Override
	protected String getValor() {
		return this.valor;
	}

}
