package edu.uclm.esi.disoft.patrones.ejercicio2;

public class Entero extends ElementoListable {
	private int valor;
	
	public Entero(int valor) {
		this.valor=valor;
	}
	
	@Override
	protected String getValor() {
		return this.valor<=0 ? "(" + this.valor + ")" : "" + this.valor;
	}
}
