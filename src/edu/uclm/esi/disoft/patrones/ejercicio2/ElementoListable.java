package edu.uclm.esi.disoft.patrones.ejercicio2;

public abstract class ElementoListable {

	private ElementoListable siguiente;

	public void setSiguiente(ElementoListable siguiente) {
		this.siguiente=siguiente;
	}

	public String getElementos() {
		Class<?> tipo=this.getClass();
		String r=this.getValor();
		if (siguiente==null)
			return r;
		return r + " - " + getElementos(tipo);
	}

	private String getElementos(Class<?> tipo) {
		String r="";
		if (siguiente==null)
			return r;
		if (siguiente.getClass()==tipo) 
			r = r + siguiente.getValor() + " - " + siguiente.getElementos(tipo);
		else
			r = r + siguiente.getElementos(tipo);
		return r;
	}

	protected abstract String getValor();
}
