package edu.uclm.esi.disoft.patrones.ejercicio2;

public class Doble extends ElementoListable {

	private double valor;

	public Doble(double valor) {
		this.valor=valor;
	}
	
	@Override
	protected String getValor() {
		return this.valor<=0 ? "(" + this.valor + ")" : "" + this.valor;
	}
}
