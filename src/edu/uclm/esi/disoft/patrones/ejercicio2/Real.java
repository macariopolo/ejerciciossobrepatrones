package edu.uclm.esi.disoft.patrones.ejercicio2;

public class Real extends ElementoListable {

	private float valor;

	public Real(float valor) {
		this.valor=valor;
	}
	
	@Override
	protected String getValor() {
		return this.valor<=0 ? "(-" + this.valor + ")" : "" + this.valor;
	}
}
