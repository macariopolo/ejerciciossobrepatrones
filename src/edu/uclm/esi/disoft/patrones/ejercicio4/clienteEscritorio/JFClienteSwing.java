package edu.uclm.esi.disoft.patrones.ejercicio4.clienteEscritorio;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.websocket.DeploymentException;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JEditorPane;
import java.awt.event.ActionListener;
import java.net.URISyntaxException;
import java.awt.event.ActionEvent;

public class JFClienteSwing extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNombre;
	private JTextField textFieldMensaje;
	private JLabel lblError;
	private JButton btnConectar;
	private JEditorPane editorPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFClienteSwing frame = new JFClienteSwing();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFClienteSwing() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 571, 330);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setText("pepe");
		textFieldNombre.setBounds(37, 6, 130, 26);
		contentPane.add(textFieldNombre);
		textFieldNombre.setColumns(10);
		
		btnConectar = new JButton("Conectar");
		btnConectar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (btnConectar.getText().equals("Conectar"))
					Proxy.get().conectar(JFClienteSwing.this, textFieldNombre.getText());
				else
					Proxy.get().desconectar();
			}
		});
		btnConectar.setBounds(179, 6, 179, 29);
		contentPane.add(btnConectar);
		
		JLabel lblMensaje = new JLabel("Mensaje:");
		lblMensaje.setBounds(47, 44, 61, 16);
		contentPane.add(lblMensaje);
		
		textFieldMensaje = new JTextField();
		textFieldMensaje.setText("Hola");
		textFieldMensaje.setBounds(120, 39, 297, 26);
		contentPane.add(textFieldMensaje);
		textFieldMensaje.setColumns(10);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Proxy.get().enviar(textFieldMensaje.getText());
			}
		});
		btnEnviar.setBounds(416, 39, 117, 29);
		contentPane.add(btnEnviar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(37, 84, 496, 167);
		contentPane.add(scrollPane);
		
		editorPane = new JEditorPane();
		scrollPane.setViewportView(editorPane);
		
		lblError = new JLabel("");
		lblError.setBounds(37, 261, 496, 16);
		contentPane.add(lblError);
	}

	public void showConectado(boolean conectado, String... id) {
		this.btnConectar.setText(conectado ? "Desconectar (" + id[0] + ")"  : "Conectar");
	}

	public void showError(String mensaje) {
		lblError.setText(mensaje);
	}

	public void showMensaje(String mensaje) {
		editorPane.setText(mensaje + "\n" + editorPane.getText());
	}
}
