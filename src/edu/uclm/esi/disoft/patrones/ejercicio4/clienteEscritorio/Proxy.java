package edu.uclm.esi.disoft.patrones.ejercicio4.clienteEscritorio;

import java.io.IOException;
import java.net.URI;

import javax.websocket.ClientEndpoint;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

@ClientEndpoint
public class Proxy {
	private JFClienteSwing ventana;
	private Session session;

	public void conectar(JFClienteSwing ventana, String userName) {
		this.ventana=ventana;
		try {
			WebSocketContainer container=ContainerProvider.getWebSocketContainer();
			container.connectToServer(this, new URI("ws://localhost:8080/servidorWSEjercicio4/ServidorWS?user=" + userName));
		}
		catch (Exception e) {
			this.ventana.showError(e.toString());
		}
	}
	
	@OnOpen
	public void onOpen(Session session) {
		this.session=session;
		this.ventana.showConectado(true, session.getId());		
	}
	
	@OnClose
	public void onClose(Session session) {
		this.ventana.showConectado(false);
	}
	
	@OnMessage
	public void onMessage(Session session, String mensaje) {
		this.ventana.showMensaje(mensaje);
	}
	
	private static class ProxyHolder {
		static Proxy singleton = new Proxy();
	}
	
	public static Proxy get() {
		return ProxyHolder.singleton;
	}

	public void enviar(String mensaje) {
		try {
			this.session.getBasicRemote().sendText(mensaje);
		} catch (Throwable e) {
			this.ventana.showError(e.toString());
		}
	}

	public void desconectar() {
		try {
			this.session.close();
		} catch (IOException e) {
			this.ventana.showError(e.toString());
		}
	}
}
