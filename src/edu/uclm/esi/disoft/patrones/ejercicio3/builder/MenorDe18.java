package edu.uclm.esi.disoft.patrones.ejercicio3.builder;

public class MenorDe18 extends Cliente {

	public MenorDe18(String nombre) {
		super(nombre);
		this.cuenta=new CuentaInfantil();
		this.tarjetaDebito=null;
		this.tarjetaCredito=null;
	}

}
