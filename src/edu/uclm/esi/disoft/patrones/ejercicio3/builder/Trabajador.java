package edu.uclm.esi.disoft.patrones.ejercicio3.builder;

public class Trabajador extends Cliente {

	public Trabajador(String nombre) {
		super(nombre);
		this.cuenta=new CuentaGuay();
		this.tarjetaDebito=new TarjetaDebitoGuay();
		this.tarjetaCredito=new TarjetaCreditoGuay();
	}

}
