package edu.uclm.esi.disoft.patrones.ejercicio3.builder;

public class Estudiante extends Cliente {

	public Estudiante(String nombre) {
		super(nombre);
		this.cuenta=new CuentaJoven();
		this.tarjetaDebito=new TarjetaDebitoJoven();
		this.tarjetaCredito=null;
	}

}
