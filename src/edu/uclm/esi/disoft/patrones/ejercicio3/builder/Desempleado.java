package edu.uclm.esi.disoft.patrones.ejercicio3.builder;

public class Desempleado extends Cliente {

	public Desempleado(String nombre) {
		super(nombre);
		this.cuenta=new CuentaSinComisiones();
		this.tarjetaDebito=new TarjetaDebitoSinComisiones();
		this.tarjetaCredito=null;
	}

}
