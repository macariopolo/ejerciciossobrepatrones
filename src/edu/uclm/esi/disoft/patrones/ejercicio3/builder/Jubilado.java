package edu.uclm.esi.disoft.patrones.ejercicio3.builder;

public class Jubilado extends Cliente {

	public Jubilado(String nombre) {
		super(nombre);
		this.cuenta=new CuentaOro();
		this.tarjetaDebito=new TarjetaDebitoOro();
		this.tarjetaCredito=new TarjetaCreditoOro();
	}

}
