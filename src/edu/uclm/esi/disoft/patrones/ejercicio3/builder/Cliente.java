package edu.uclm.esi.disoft.patrones.ejercicio3.builder;

public abstract class Cliente {
	protected String nombre;
	protected Cuenta cuenta;
	protected TarjetaDebito tarjetaDebito;
	protected TarjetaCredito tarjetaCredito;
	
	public Cliente(String nombre) {
		this.nombre=nombre;
	}
}
