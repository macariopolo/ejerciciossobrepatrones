package edu.uclm.esi.disoft.patrones.ejercicio3.abstractFactory;

public class FabricaMenorDe18 extends FabricaClientes {

	@Override
	public Cliente buildCliente(String nombre) {
		Cliente cliente=new MenorDe18(nombre);
		return cliente;
	}

}
