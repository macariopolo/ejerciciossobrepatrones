package edu.uclm.esi.disoft.patrones.ejercicio3.abstractFactory;

public class FabricaTrabajador extends FabricaClientes {

	@Override
	public Cliente buildCliente(String nombre) {
		Cliente cliente=new Estudiante(nombre);
		TarjetaDebito debito=new TarjetaDebitoGuay();
		TarjetaCredito credito=new TarjetaCreditoGuay();
		cliente.setTarjetaDebito(debito);
		cliente.setTarjetaCredito(credito);
		return cliente;
	}

}
