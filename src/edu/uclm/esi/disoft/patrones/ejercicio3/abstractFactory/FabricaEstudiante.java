package edu.uclm.esi.disoft.patrones.ejercicio3.abstractFactory;

public class FabricaEstudiante extends FabricaClientes {

	@Override
	public Cliente buildCliente(String nombre) {
		Cliente cliente=new Estudiante(nombre);
		TarjetaDebito debito=new TarjetaDebitoJoven();
		cliente.setTarjetaDebito(debito);
		return cliente;
	}

}
