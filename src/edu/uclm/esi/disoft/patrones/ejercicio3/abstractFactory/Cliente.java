package edu.uclm.esi.disoft.patrones.ejercicio3.abstractFactory;

public abstract class Cliente {
	protected String nombre;
	protected Cuenta cuenta;
	protected TarjetaDebito tarjetaDebito;
	protected TarjetaCredito tarjetaCredito;
	
	public Cliente(String nombre) {
		this.nombre=nombre;
	}

	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	public TarjetaDebito getTarjetaDebito() {
		return tarjetaDebito;
	}

	public void setTarjetaDebito(TarjetaDebito tarjetaDebito) {
		this.tarjetaDebito = tarjetaDebito;
	}

	public TarjetaCredito getTarjetaCredito() {
		return tarjetaCredito;
	}

	public void setTarjetaCredito(TarjetaCredito tarjetaCredito) {
		this.tarjetaCredito = tarjetaCredito;
	}
	
	
}
