package edu.uclm.esi.disoft.patrones.ejercicio3.abstractFactory;

public abstract class FabricaClientes {
	public abstract Cliente buildCliente(String nombre);
}
