package edu.uclm.esi.disoft.patrones.ejercicio3.inyeccionDependencias;

import java.io.File;
import java.io.FileInputStream;

import org.json.JSONArray;
import org.json.JSONObject;

public abstract class Cliente {
	protected String nombre;
	protected Cuenta cuenta;
	protected TarjetaDebito tarjetaDebito;
	protected TarjetaCredito tarjetaCredito;
	
	public Cliente(String nombre) {
		this.nombre=nombre;
		try {
			crearProductos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void crearProductos() throws Exception {
		String directorio=new File(".").getAbsolutePath();
		String fichero =directorio + "/src/edu/uclm/esi/disoft/patrones/ejercicio3/inyeccionDependencias/productosBancarios.txt";
		FileInputStream fis=new FileInputStream(fichero);
		byte[] b=new byte[fis.available()];
		fis.read(b);
		fis.close();
		String texto=new String(b);
		JSONArray jsa=new JSONArray(texto);
		String clase=this.getClass().getSimpleName();
		JSONObject combinacion=null;
		for (int i=0; i<jsa.length(); i++) {
			combinacion=jsa.getJSONObject(i);
			if (combinacion.getString("cliente").equals(clase)) {
				break;
			}
		}
		this.cuenta=(Cuenta) Class.forName("edu.uclm.esi.disoft.patrones.ejercicio3.inyeccionDependencias." + combinacion.getString("cuenta")).newInstance();
		if (combinacion.optString("tarjetaDebito").length()>0)
			this.tarjetaDebito=(TarjetaDebito) Class.forName("edu.uclm.esi.disoft.patrones.ejercicio3.inyeccionDependencias." + combinacion.getString("tarjetaDebito")).newInstance();
		if (combinacion.optString("tarjetaCredito").length()>0)
			this.tarjetaCredito=(TarjetaCredito) Class.forName("edu.uclm.esi.disoft.patrones.ejercicio3.inyeccionDependencias." + combinacion.getString("tarjetaCredito")).newInstance();
	}
}
